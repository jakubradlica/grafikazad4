package pl.radlica.model;

import java.util.ArrayList;

public class Triangle {

    public Point3D normal;

    public ArrayList<Point3D> points = new ArrayList<Point3D>();

    public Triangle(Point3D pointA, Point3D pointB, Point3D pointC){

        points.add(pointA);
        points.add(pointB);
        points.add(pointC);
    }
    public Triangle(Point3D normal, Point3D pointA, Point3D pointB, Point3D pointC){
        this.normal=normal;
        points.add(pointA);
        points.add(pointB);
        points.add(pointC);
    }
}
