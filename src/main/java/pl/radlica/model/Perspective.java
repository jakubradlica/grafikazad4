package pl.radlica.model;

public enum Perspective {
    X,
    Z,
    Y
}
