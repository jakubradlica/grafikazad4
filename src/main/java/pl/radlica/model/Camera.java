package pl.radlica.model;


import javax.vecmath.Vector3d;
import java.util.ArrayList;

public class Camera {

    Point3D eye;
    Point3D canvas;
    Float angle;
    Vector3d ecVector;
    Vector3d normalVector;
    Vector3d normalVector2;
    int camSize = 1;

    public ArrayList<Point3D> corners = new ArrayList<Point3D>();

    public ArrayList<Point3D> getCorners() {
        return corners;
    }

    public Camera(Point3D eye, Point3D canvas, Float angle){
        this.eye=eye;
        this.canvas=canvas;
        this.angle=angle;
        calcEyeCanvasVector();
        calcNormalVector();
        calcNormalVector2();
        calcCorners();
    }

    public void calcEyeCanvasVector(){
        double deltaX = getCanvas().getX()-getEye().getX();
        double deltaY = getCanvas().getY()-getEye().getY();
        double deltaZ = getCanvas().getZ()-getEye().getZ();
        this.ecVector=new Vector3d(deltaX, deltaY, deltaZ);
    }

    public void calcNormalVector(){
        double nX=5, nY=1;
        double nZ = (nX*ecVector.x+nY*ecVector.y)/(-1*ecVector.z);
        this.normalVector = new Vector3d(nX, nY, nZ);
        normalVector.normalize();
    }

    public void calcNormalVector2(){
        double nX=10, nY=-3;
        double nZ = (nX*(ecVector.x+normalVector.x)+nY*(ecVector.y+normalVector.y))/(-1*(ecVector.z+normalVector.z));
        this.normalVector2 = new Vector3d(nX, nY, nZ);
        this.normalVector2.normalize();
        this.normalVector2.cross(ecVector, normalVector);
        this.normalVector2.normalize();
    }

    public void calcCorners() {

        double scale =10.0;
        if(corners.isEmpty()) {
            corners.add(new Point3D(canvas.x + normalVector.x*scale, canvas.y + normalVector.y*scale, canvas.z + normalVector.z*scale));
            corners.add(new Point3D(canvas.x - normalVector.x*scale, canvas.y - normalVector.y*scale, canvas.z - normalVector.z*scale));
            corners.add(new Point3D(canvas.x + normalVector2.x*scale, canvas.y + normalVector2.y*scale, canvas.z + normalVector2.z*scale));
            corners.add(new Point3D(canvas.x - normalVector2.x*scale, canvas.y - normalVector2.y*scale, canvas.z - normalVector2.z*scale));
        } else {
            corners.set(0, new Point3D(canvas.x + normalVector.x, canvas.y + normalVector.y*scale, canvas.z + normalVector.z*scale));
            corners.set(1, new Point3D(canvas.x - normalVector.x*scale, canvas.y - normalVector.y*scale, canvas.z - normalVector.z*scale));
            corners.set(2, new Point3D(canvas.x + normalVector2.x*scale, canvas.y + normalVector2.y*scale, canvas.z + normalVector2.z*scale));
            corners.set(3, new Point3D(canvas.x - normalVector2.x*scale , canvas.y - normalVector2.y*scale, canvas.z - normalVector2.z*scale));
        }

        for(Point3D p: corners){
            System.out.println("("+p.getX()+","+p.getY()+","+p.getZ()+")");
        }
//        for(int i=0; i<corners.size(); i++){
//            for(int j=0; j<corners.size(); j++){
//                double xSquare = Math.pow(corners.get(i).getX()-corners.get(j).getX(),2);
//                double ySquare = Math.pow(corners.get(i).getY()-corners.get(j).getY(),2);
//                double zSquare = Math.pow(corners.get(i).getZ()-corners.get(j).getZ(),2);
//                double length = Math.sqrt(xSquare+ySquare+zSquare);
//                System.out.println("Odległość pomiędzy p"+i+" i p"+j+" "+length);
//            }
//        }
    }


    public Point3D getEye() {
        return eye;
    }

    public void setEye(Point3D eye) {
        this.eye = eye;
    }

    public Point3D getCanvas() {
        return canvas;
    }

    public void setCanvas(Point3D canvas) {
        this.canvas = canvas;
    }

    public Float getAngle() {
        return angle;
    }

    public void setAngle(Float angle) {
        this.angle = angle;
    }

}
