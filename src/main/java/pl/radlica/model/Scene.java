package pl.radlica.model;

import org.javatuples.Triplet;

import java.awt.*;
import java.util.ArrayList;

public class Scene {

    public ArrayList<Point3D> points;
    public ArrayList<Triplet<Integer,Integer,Integer>> vertexIndexesOfTriangle;

    public Scene(ArrayList<Point3D> points,
                 ArrayList<Triplet<Integer,Integer,Integer>> vertexIndexesOfTriangle){
        this.points=points;
        this.vertexIndexesOfTriangle=vertexIndexesOfTriangle;
    }

    public ArrayList<Triangle> getTriangles() {
        ArrayList<Triangle> triangles = new ArrayList<Triangle>();
        for(Triplet<Integer,Integer,Integer> vertexIndexes: vertexIndexesOfTriangle){
            triangles.add(new Triangle(points.get(vertexIndexes.getValue0()),
                    points.get(vertexIndexes.getValue1()),
                    points.get(vertexIndexes.getValue2())));
        }

        return triangles;
    }
}
