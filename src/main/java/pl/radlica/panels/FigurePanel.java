package pl.radlica.panels;

import pl.radlica.model.Camera;
import pl.radlica.model.Perspective;
import pl.radlica.model.Point3D;
import pl.radlica.model.Triangle;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;

public class FigurePanel extends JPanel implements MouseListener, MouseMotionListener {

    private ArrayList<Triangle> triangles = new ArrayList<Triangle>();
    Perspective perspective = Perspective.Z;
    int scale = 10;
    private Camera camera;
    private boolean mouseDragged = false;
    private double mouseX;
    private double mouseY;

    public FigurePanel() {
        setBackground(Color.BLACK);
        addMouseListener(this);
        addMouseMotionListener(this);
//        setSize(new Dimension(100,100));
    }

    public void setParameters(ArrayList<Triangle> triangles, Perspective perspective) {
        this.triangles = triangles;
        this.perspective = perspective;
    }

    public void setCamera(Camera camera) {
        this.camera = camera;
    }

    public void setTriangles(ArrayList<Triangle> triangles) {
        this.triangles = triangles;
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        g.setColor(Color.WHITE);
        Graphics2D g2 = (Graphics2D) g;
        g2.setStroke(new BasicStroke(1));

        g2.setRenderingHint(
                RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);

        if (triangles.size() > 0) {
            for (Triangle t : triangles) {
                drawTriangleAtGraphics(g2, t);
            }
        }

        drawCameraAtGraphics(g2, camera);

    }

    private void drawCameraAtGraphics(Graphics2D g2, Camera camera) {
        if (camera != null) {
            Color originalColor = g2.getColor();
            double cameraPointSize = 10;
            switch (perspective) {
                case Z:
                    //Rysowanie kwadratu kamery
                    g2.setColor(Color.RED);
                    for (Point3D pointA : camera.getCorners()) {
                        for (Point3D pointB : camera.getCorners()) {
                            g2.drawLine((int) pointA.getX() * scale, (int) pointA.getY() * scale, (int) pointB.getX() * scale, (int) pointB.getY() * scale);
                        }
                    }

                    //Rysowanie prostych do rogów kamery
                    g2.setColor(Color.CYAN);
                    for (Point3D point : camera.getCorners()) {
                        g2.drawLine((int) camera.getEye().getX() * scale, (int) camera.getEye().getY() * scale, (int) point.getX() * scale, (int) point.getY() * scale);
                    }

                    //Rysowanie punktów zaczepienia i wektora kamery
                    g2.setColor(Color.YELLOW);
                    g2.fillOval((int) camera.getEye().getX() * scale - (int)cameraPointSize/2, (int) camera.getEye().getY() * scale - (int)cameraPointSize/2, (int)cameraPointSize, (int)cameraPointSize);
                    g2.setColor(Color.ORANGE);
                    g2.fillOval((int) camera.getCanvas().getX() * scale - (int)cameraPointSize/2, (int) camera.getCanvas().getY() * scale - (int)cameraPointSize/2, (int)cameraPointSize, (int)cameraPointSize);
                    g2.setColor(Color.BLUE);
                    g2.drawLine((int) camera.getEye().getX() * scale, (int) camera.getEye().getY() * scale, (int) camera.getCanvas().getX() * scale, (int) camera.getCanvas().getY() * scale);
                    break;
                case Y:
                    //Rysowanie kwadratu kamery
                    g2.setColor(Color.RED);
                    for (Point3D pointA : camera.getCorners()) {
                        for (Point3D pointB : camera.getCorners()) {
                            g2.drawLine((int) pointA.getX() * scale, this.getHeight() - (int) pointA.getZ() * scale, (int) pointB.getX() * scale, this.getHeight() - (int) pointB.getZ() * scale);
                        }
                    }

                    //Rysowanie prostych do rogów kamery
                    g2.setColor(Color.CYAN);
                    for (Point3D point : camera.getCorners()) {
                        g2.drawLine((int) camera.getEye().getX() * scale, this.getHeight() - (int) camera.getEye().getZ() * scale, (int) point.getX() * scale, this.getHeight() - (int) point.getZ() * scale);
                    }

                    //Rysowanie punktów zaczepienia i wektora kamery
                    g2.setColor(Color.YELLOW);
                    g2.fillOval((int) camera.getEye().getX() * scale - (int)cameraPointSize/2, this.getHeight() - (int) camera.getEye().getZ() * scale - 5, (int)cameraPointSize, (int)cameraPointSize);
                    g2.setColor(Color.ORANGE);
                    g2.fillOval((int) camera.getCanvas().getX() * scale - (int)cameraPointSize/2, this.getHeight() - (int) camera.getCanvas().getZ() * scale - 5, (int)cameraPointSize, (int)cameraPointSize);
                    g2.setColor(Color.BLUE);
                    g2.drawLine((int) camera.getEye().getX() * scale, this.getHeight() - (int) camera.getEye().getZ() * scale, (int) camera.getCanvas().getX() * scale, this.getHeight() - (int) camera.getCanvas().getZ() * scale);

                    break;
                case X:
                    //Rysowanie kwadratu kamery
                    g2.setColor(Color.RED);
                    for (Point3D pointA : camera.getCorners()) {
                        for (Point3D pointB : camera.getCorners()) {
                            g2.drawLine(this.getWidth() - (int) pointA.getZ() * scale, this.getHeight() - (int) pointA.getY() * scale, this.getWidth() - (int) pointB.getZ() * scale, this.getHeight() - (int) pointB.getY() * scale);
                        }
                    }
                    //Rysowanie prostych do rogów kamery
                    g2.setColor(Color.CYAN);
                    for (Point3D point : camera.getCorners()) {
                        g2.drawLine(this.getWidth() - (int) camera.getEye().getZ() * scale, this.getHeight() - (int) camera.getEye().getY() * scale, this.getWidth() - (int) point.getZ() * scale, this.getHeight() - (int) point.getY() * scale);
                    }

                    //Rysowanie punktów zaczepienia i wektora kamery
                    g2.setColor(Color.YELLOW);
                    g2.fillOval(this.getWidth() - (int) camera.getEye().getZ() * scale - (int)cameraPointSize/2, this.getHeight() - (int) camera.getEye().getY() * scale - (int)cameraPointSize/2, (int)cameraPointSize, (int)cameraPointSize);
                    g2.setColor(Color.ORANGE);
                    g2.fillOval(this.getWidth() - (int) camera.getCanvas().getZ() * scale - (int)cameraPointSize/2, this.getHeight() - (int) camera.getCanvas().getY() * scale - (int)cameraPointSize/2, (int)cameraPointSize, (int)cameraPointSize);
                    g2.setColor(Color.BLUE);
                    g2.drawLine(this.getWidth() - (int) camera.getEye().getZ() * scale, this.getHeight() - (int) camera.getEye().getY() * scale, this.getWidth() - (int) camera.getCanvas().getZ() * scale, this.getHeight() - (int) camera.getCanvas().getY() * scale);
                    break;
            }
            g2.setColor(originalColor);
        }
    }

    public void drawTriangleAtGraphics(Graphics graphics, Triangle triangle) {
        switch (perspective) {
            case Z:
                graphics.drawLine((int) triangle.points.get(0).x * scale, (int) triangle.points.get(0).y * scale, (int) triangle.points.get(1).x * scale, (int) triangle.points.get(1).y * scale);
                graphics.drawLine((int) triangle.points.get(0).x * scale, (int) triangle.points.get(0).y * scale, (int) triangle.points.get(2).x * scale, (int) triangle.points.get(2).y * scale);
                graphics.drawLine((int) triangle.points.get(2).x * scale, (int) triangle.points.get(2).y * scale, (int) triangle.points.get(1).x * scale, (int) triangle.points.get(1).y * scale);
                break;

            case Y:
                graphics.drawLine((int) triangle.points.get(0).x * scale, this.getHeight() - (int) triangle.points.get(0).z * scale, (int) triangle.points.get(1).x * scale, this.getHeight() - (int) triangle.points.get(1).z * scale);
                graphics.drawLine((int) triangle.points.get(0).x * scale, this.getHeight() - (int) triangle.points.get(0).z * scale, (int) triangle.points.get(2).x * scale, this.getHeight() - (int) triangle.points.get(2).z * scale);
                graphics.drawLine((int) triangle.points.get(2).x * scale, this.getHeight() - (int) triangle.points.get(2).z * scale, (int) triangle.points.get(1).x * scale, this.getHeight() - (int) triangle.points.get(1).z * scale);
                break;

            case X:
                graphics.drawLine(this.getHeight() - (int) triangle.points.get(0).y * scale, this.getHeight() - (int) triangle.points.get(0).z * scale, this.getHeight() - (int) triangle.points.get(1).y * scale, this.getHeight() - (int) triangle.points.get(1).z * scale);
                graphics.drawLine(this.getHeight() - (int) triangle.points.get(0).y * scale, this.getHeight() - (int) triangle.points.get(0).z * scale, this.getHeight() - (int) triangle.points.get(2).y * scale, this.getHeight() - (int) triangle.points.get(2).z * scale);
                graphics.drawLine(this.getHeight() - (int) triangle.points.get(2).y * scale, this.getHeight() - (int) triangle.points.get(2).z * scale, this.getHeight() - (int) triangle.points.get(1).y * scale, this.getHeight() - (int) triangle.points.get(1).z * scale);
                break;
        }
    }

    public void mouseDragged(MouseEvent e) { ;
        if(mouseDragged){
            draggedAtCameraPoint(e.getX(), e.getY());
            System.out.println("D: "+e.getX()+" "+e.getY());
            this.revalidate();
            this.repaint();
        }
    }

    public void mouseMoved(MouseEvent e) {
//        System.out.println("M: "+e.getX()+" "+e.getY());
//        draggedAtCameraPoint(e.getX(), e.getY());
    }



    public void draggedAtCameraPoint(int x, int y) {
        switch (perspective) {
            case Z:
                double d = Math.hypot(x-camera.getEye().getX()*scale, y-camera.getEye().getY()*scale);
                double d2 = Math.hypot(x-camera.getCanvas().getX()*scale, y-camera.getCanvas().getY()*scale);
                if(d<=10){
                    camera.getEye().setX(x/scale);
                    camera.getEye().setY(y/scale);
                    System.out.println("New Eye: "+camera.getEye().getX()+" "+camera.getEye().getY());
                } else if(d2<=10){
                    camera.getCanvas().setX(x/scale);
                    camera.getCanvas().setY(y/scale);
                    System.out.println("New Canvas: "+camera.getCanvas().getX()+" "+camera.getCanvas().getY());
                }
                break;
            case Y:
                break;
            case X:
                break;

        }
    }


    public void mouseClicked(MouseEvent e) {
        System.out.println("C: " + e.getX()+" "+e.getY());
    }

    public void mousePressed(MouseEvent e) {
        mouseDragged=true;
    }

    public void mouseReleased(MouseEvent e) {
        mouseDragged=false;
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }
}
