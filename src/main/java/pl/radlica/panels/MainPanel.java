package pl.radlica.panels;

import pl.radlica.model.Camera;
import pl.radlica.model.Point3D;
import pl.radlica.repository.SceneRepository;
import pl.radlica.model.Perspective;
import pl.radlica.model.Triangle;

import javax.swing.*;
import java.io.IOException;
import java.util.ArrayList;

public class MainPanel {
    public JPanel mainPanel;
    private FigurePanel perspectiveOne;
    private FigurePanel perspectiveTwo;
    private FigurePanel perspectiveThree;
    private JButton buttonLoadScene;
    private JSlider angleOfView;
    private JButton buttonSaveScene;

    private ArrayList<Triangle> triangles = new ArrayList();
    private Camera camera;

    public MainPanel() throws IOException {
        initDefaultValues();
}

    private void initDefaultValues() throws IOException {
        this.camera = new Camera(new Point3D(20,10,20), new Point3D(20,20,20), 80f);
        SceneRepository sceneRepository = new SceneRepository();
        this.triangles = sceneRepository.getDefaultScene();

        perspectiveOne.setParameters(triangles, Perspective.X);
        perspectiveOne.setCamera(this.camera);
        perspectiveTwo.setParameters(triangles, Perspective.Y);
        perspectiveTwo.setCamera(this.camera);
        perspectiveThree.setParameters(triangles, Perspective.Z);
        perspectiveThree.setCamera(this.camera);

//        this.camera=new Camera(new Point3D(50,50,50), new Point3D(200,200,200), (float) 89);
    }

}
