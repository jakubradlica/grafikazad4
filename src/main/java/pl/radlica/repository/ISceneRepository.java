package pl.radlica.repository;

import pl.radlica.model.Scene;

import java.io.FileNotFoundException;
import java.io.IOException;

public interface ISceneRepository {

    public Scene loadSceneFromFilepath(String filepath) throws IOException;
    public void saveSceneToFile(String filepath);
}
