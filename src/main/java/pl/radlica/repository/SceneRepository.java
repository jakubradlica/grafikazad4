package pl.radlica.repository;

import org.javatuples.Triplet;
import pl.radlica.model.Point3D;
import pl.radlica.model.Scene;
import pl.radlica.model.Triangle;

import java.io.*;
import java.util.ArrayList;

public class SceneRepository implements ISceneRepository{

    public static String TEA_BRS_PATH = "C:\\Users\\kamo\\Desktop\\TEA.BRS";

    public  ArrayList<Triangle> loadSceneTrianglesFromFilepath(String filePath) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(new File(filePath)));

        String line;

        do {
            line = br.readLine();
        } while (!line.matches("[0-9]+\\s*?"));

        String lineWithPointsCount = line.replaceAll("\\s+","");
        int pointsCount = Integer.parseInt(lineWithPointsCount);

        ArrayList<Point3D> points = new ArrayList<Point3D>(pointsCount);
        for(int i=0; i<pointsCount; i++){
            String lineWithPoints = br.readLine();
            String[] componentsOfThePoint = lineWithPoints.split(" ");
            double x = Double.parseDouble(componentsOfThePoint[0])+20;
            double y = Double.parseDouble(componentsOfThePoint[1])+20;
            double z = Double.parseDouble(componentsOfThePoint[2])+20;
            Point3D point = new Point3D(x, y, z);
            points.add(point);
        }
        do {
            line = br.readLine();
        } while (!line.matches("[0-9]+\\s*?"));

        String lineWithTrianglesCount = line.replaceAll("\\s+","");
        int trianglesCount = Integer.parseInt(lineWithTrianglesCount);

        ArrayList<Triangle> triangles = new ArrayList<Triangle>(trianglesCount);
        for(int i=0; i<trianglesCount; i++){
            String lineWithVertexIndexes = br.readLine();
            String[] vertexIndexes = lineWithVertexIndexes.split(" ");
            int p1 = Integer.parseInt(vertexIndexes[0]);
            int p2 = Integer.parseInt(vertexIndexes[1]);
            int p3 = Integer.parseInt(vertexIndexes[2]);
            Triangle triangle = new Triangle(points.get(p1), points.get(p2), points.get(p3));
            triangles.add(triangle);
        }

        return triangles;
    }

    public  ArrayList<Triangle> getDefaultScene(){
        ArrayList<Point3D> points = new ArrayList<Point3D>();
        ArrayList<Triangle> triangles = new ArrayList<Triangle>();

        points.add(new Point3D(4,8, 1));
        points.add(new Point3D(8,8, 1));
        points.add(new Point3D(8,4, 1));
        points.add(new Point3D(4,4, 1));

        points.add(new Point3D(1,11, 6));
        points.add(new Point3D(11,11, 6));
        points.add(new Point3D(11,1, 6));
        points.add(new Point3D(1,1, 6));

        triangles.add(createTriangle(0,5, 4, points));
        triangles.add(createTriangle(0,1, 5, points));
        triangles.add(createTriangle(0,4, 7, points));
        triangles.add(createTriangle(0,3, 7, points));
        triangles.add( createTriangle(0,3, 2, points));
        triangles.add(createTriangle(0,1, 2, points));
        triangles.add( createTriangle(5,1, 6, points));
        triangles.add( createTriangle(2,1, 6, points));
        triangles.add(createTriangle(2,7, 6, points));
        triangles.add(createTriangle(2,7, 3, points));
        triangles.add(createTriangle(6,7, 5, points));
        triangles.add(createTriangle(4,7, 5, points));
        return triangles;

    }

    private Triangle createTriangle(int i, int y, int x, ArrayList<Point3D> points)
    {
        return new Triangle(points.get(i), points.get(y), points.get(x));
    }

    public Scene loadSceneFromFilepath(String filePath) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(new File(filePath)));
        String line;
        do {
            line = br.readLine();
        } while (!line.matches("[0-9]+\\s*?"));

        String lineWithPointsCount = line.replaceAll("\\s+","");
        int pointsCount = Integer.parseInt(lineWithPointsCount);

        ArrayList<Point3D> points = new ArrayList<Point3D>(pointsCount);
        for(int i=0; i<pointsCount; i++){
            String lineWithPoints = br.readLine();
            String[] componentsOfThePoint = lineWithPoints.split(" ");
            double x = Double.parseDouble(componentsOfThePoint[0])+20;
            double y = Double.parseDouble(componentsOfThePoint[1])+20;
            double z = Double.parseDouble(componentsOfThePoint[2])+20;
            Point3D point = new Point3D(x, y, z);
            points.add(point);
        }
        do {
            line = br.readLine();
        } while (!line.matches("[0-9]+\\s*?"));

        String lineWithTrianglesCount = line.replaceAll("\\s+","");
        int trianglesCount = Integer.parseInt(lineWithTrianglesCount);

        ArrayList<Triplet<Integer,Integer,Integer>>  vertexIndexesOfTriangle = new ArrayList();
        for(int i=0; i<trianglesCount; i++){
            String lineWithVertexIndexes = br.readLine();
            String[] vertexIndexes = lineWithVertexIndexes.split(" ");
            int p1 = Integer.parseInt(vertexIndexes[0]);
            int p2 = Integer.parseInt(vertexIndexes[1]);
            int p3 = Integer.parseInt(vertexIndexes[2]);
            vertexIndexesOfTriangle.add(new Triplet<Integer, Integer, Integer>(p1,p2,p3));
        }

        return new Scene(points, vertexIndexesOfTriangle);
    }

    public void saveSceneToFile(String filepath) {

    }
}
