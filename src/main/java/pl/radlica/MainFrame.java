package pl.radlica;

import pl.radlica.panels.MainPanel;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;


public class MainFrame extends JFrame {

    MainPanel mainPanel;

    public MainFrame() throws IOException {
        this.mainPanel = new MainPanel();
        this.setContentPane(this.mainPanel.mainPanel);
        this.setSize(new Dimension(800,640));
    }
}
